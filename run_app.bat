@echo off

set display=localhost:0.0
REM 事前に一度VcXsrvを起動して設定ファイルを作成しておく
set xconfig="%CD%\config.xlaunch"
set command=umask 0022; emacs -d %display% --chdir ${HOME}
REM ディストリビューション名のコマンドでWSLを実行しないとオプションを上手く受け取れないようだ
REM set shell=%SYSTEMROOT%\System32\wsl.exe
set shell=debian

REM VcXsrvの起動確認
REM 起動していなかったら起動する
tasklist | find "vcxsrv.exe" > NUL
if not %ERRORLEVEL% == 0 (
    start "%PROGRAMFILES%\VcXsrv\xlaunch.exe" %xconfig%
)

REM %shell% "%command%"
%shell% run "%command%"

exit