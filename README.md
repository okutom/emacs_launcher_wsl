# Emacs Launcher for WSL

Launch emacs in a graphical window without console windows.  

## Requirement

- Windows Subsystem for Linux  
    - xserver  
    - emacs  
- VcXsrv  

## How to USE

1. Clone the repository.  
1. Start VcXsrv once and create a configuration file.  
1. Move the configuration file to `/path/to/clone/this/emacs_launcher_wsl`.  
1. Create the shortcut link to hide.vbs.  
1. Open created shortcut link properties.  
Edit the link target.  

```
wscript /path/to/hide.vbs /path/to/run_app.bat
```

## Caution
Many configurations are hard coded.  
Please change configurations to apply your environemnt.  

### run_app.bat
- display  
This acts like environment variable DISPLAY.  
- xconfig  
/path/to/your.xlaunch  
- command  
The command you would like to execute.  
- shell  
Your WSL command.  
I recommend distribution-name-exe-file.  
e.g. `ubuntu`, `debian`, etc.  
- line 13 to 16  
run_app.bat uses VcXsrv.  
If you use any other Xserver, Please change those lines.  

